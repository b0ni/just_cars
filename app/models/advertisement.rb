class Advertisement < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  has_one_attached :photo
  monetize :price_cents

  mapping do
    indexes :title
    indexes :description
    indexes :price, type: :float
    indexes :price_cents, type: :integer
    indexes :created_at, type: :date
  end

  def photo_url
    Rails.application.routes.url_helpers.rails_blob_url(photo) unless photo.attachment.nil?
  end
end
