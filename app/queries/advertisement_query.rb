class AdvertisementQuery
  class << self
    attr_reader :params

    def call(params = {})
      @params = params
      Advertisement.search(query).records.all
    end

    private

    def query
      result_query = { query: { bool: {}} }

      if params[:query].blank? && params[:price].blank?
        result_query[:query] = { match_all: {} }
      end

      unless params[:query].blank?
        result_query[:query][:bool][:must] = []
        must = [
          {
            multi_match:
              {
                query: params[:query],
                fields: %w(title description)
              }
          }
        ]
        result_query[:query][:bool][:must] = must
      end

      unless params[:price].blank?
        price_range = { gte: (params[:price][:min] || 0).to_i }
        price_range[:lte] = params[:price][:max].to_i if params[:price][:max]

        result_query[:query][:bool][:filter] = [ { range: { price_cents: price_range } } ]
      end

      size = (params[:size] || 10).to_i
      result_query[:from] = (params[:page] || 0).to_i * size
      result_query[:size] = size
      result_query[:sort] = sort
      result_query
    end

    def sort
      if params[:sort].present?
        [params[:sort]]
      else
        [{ created_at: :desc }]
      end
    end
  end
end
