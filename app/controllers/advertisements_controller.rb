class AdvertisementsController < ApplicationController
  def create
    advertisement = Advertisement.new(advertisements_params)
    if advertisement.save
      render_json(advertisement)
    else
      render json: advertisement.errors, status: 422
    end
  end

  def index
    render_json(AdvertisementQuery.call(search_params))
  end

  def show
    advertisement = Advertisement.find_by(id: params[:id])
    if advertisement.nil?
      head 404
    else
      render_json(advertisement)
    end
  end

  private

  def render_json(resource)
    render json: resource, methods: [:photo_url, :price]
  end

  def advertisements_params
    params.permit(:title, :description, :price, :photo)
  end

  def search_params
    params.permit(:query, :size, :page, :sort, price: [:min, :max])
  end
end
