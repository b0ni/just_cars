# Just Cars

### Od autora

Niestety nie mialem czasu zeby dopieścić tę aplikację a chcę chociaż spróbować rozmowy.

Co zostało do zrobienia:
- Testy. Co prawda, w aplikacji praktycznie nie ma kodu do testowania bo wszystko opiera się o framework, gemy i
  Elasticsearch ale jednak pasowałoby zrobić proste integracyjne testy żeby sprawdzić czy to wszystko działa po spięciu
wszystkiego razem.
- Być może przenieść tworzenie ogłoszenia do service objectu ale na tym etapie byłby to ogromny overenengineering
- Lepsze sortowanie
- rubocop
- walidacja danych
- Użycie fast_jsonapi/innego narzedzia do serializacji do json w standardzie jsonapi.org
- Dokumentacja typu swagger
- Przetestować wszystko dokładnie

Niestety życie sprawiło, ze nie miałem czasu na wykonanie zadania tak jakbym chciał je zrobić. Sam nie jestem do
zadowolony z tego zadania, więc zrozumiem jeśli nie będzie sensu kontynuowania procesu rekrutacji ze mną.

## Available Endpoints

### GET /advertisements

Returns all advertisements paginated and filtered by params

Params:
- query - value to search in title/description of advertisement
- size - page size
- page - page number
- sort - field to sort by(currently only ascending order)
- price[min] - minimal price for filtering advertisements
- price[max] - maximal price for filtering advertisements


### GET /advertisements/:id


Returns single advertisement


### POST /advertisements


Body:

- title - title of advertisement(string)
- description - description of advertisement(string)
- price - price of advertisement(number)
- photo - file with photo for advertisement
