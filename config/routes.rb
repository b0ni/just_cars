Rails.application.routes.draw do
  resources :advertisements, only: [:create, :index, :show]
end
