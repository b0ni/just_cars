class CreateAdvertisements < ActiveRecord::Migration[6.0]
  def change
    create_table :advertisements do |t|
      t.string :title
      t.text :description
      t.integer :price_cents

      t.timestamps
    end
  end
end
